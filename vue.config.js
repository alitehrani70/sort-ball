const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
  productionSourceMap: false,
  "transpileDependencies": [
    "vuetify"
  ],
  configureWebpack: {
    plugins: process.env.NODE_ENV === "development"
      ? []
      : [new TerserPlugin()],
    optimization: {
      splitChunks: {
        chunks: "all",
      },
    },
  },
  pwa: {
    name: "sort ball puzzle online game",
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "./src/service-worker.js",
      swDest: "service-worker.js",
    },
    manifestOptions: {
      name: "sort ball puzzle",
      short_name: "sort ball",
      start_url: "/",
      display: "fullscreen",
      icons: [
        {
          src: "/img/icons/android-chrome-192x192.png",
          sizes: "192x192",
          type: "image/png",
          purpose: "maskable",

        },
        {
          src: "/img/icons/android-chrome-256x256.png",
          sizes: "256x256",
          type: "image/png",
          purpose: "maskable",

        },
        {
          src: "/img/icons/android-chrome-512x512.png",
          sizes: "512x512",
          type: "image/png",
          purpose: "maskable",
        },
      ],
      theme_color: "#fff",
      background_color: "#570074",
    },
  },
}