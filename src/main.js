import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import VueGtag from "vue-gtag";
import './registerServiceWorker'
import store from './store'

Vue.config.productionTip = false
Vue.use(VueGtag, {
  config: { id: "G-M5X517M4K2" }
});

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
