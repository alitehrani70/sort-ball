import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
    filter: (mutation) => mutation.type !== "addSnakbar",
    modules: [
        "numberOfCopletedGame",
        "themeIsDark",
        "movesInTheLevel",
        "bestMoveRecord",
        "startRows"
    ],
});

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        numberOfCopletedGame: 0,
        themeIsDark: false,
        movesInTheLevel: 0,
        bestMoveRecord: 0,
        startRows: []
    },
    getters: {
        numberOfCompletedGame(state) {
            return state.numberOfCopletedGame
        },
        themeIsDark(state) {
            return state.themeIsDark
        },
        bestMoveRecord(state) {
            return state.bestMoveRecord
        },
        movesInTheLevel(state) {
            return state.movesInTheLevel
        },
        startRows(state) {
            return state.startRows
        }
    },
    mutations: {
        increaseCompletedGame(state) {
            state.numberOfCopletedGame += 1
        },
        toggleTheme(state) {
            state.themeIsDark = !state.themeIsDark
        },
        changeBestRecord(state, newRecord) {
            if (state.bestMoveRecord === 0 || state.bestMoveRecord > newRecord)
                state.bestMoveRecord = newRecord
        },
        increaseLevelMove(state) {
            state.movesInTheLevel += 1
        },
        resetLevelMove(state) {
            state.movesInTheLevel = 0
        },
        setStartRows(state, payload) {
            state.startRows = payload
        }
    },
    actions: {},
    modules: {},
    plugins: [vuexLocal.plugin],
});