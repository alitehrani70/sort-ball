workbox.setConfig({
    debug: false,
});

// Start controlling any existing clients as soon as it activates
workbox.core.clientsClaim()

// Skip over the SW waiting lifecycle stage
workbox.core.skipWaiting()

workbox.precaching.cleanupOutdatedCaches()

workbox.precaching.precacheAndRoute(self.__precacheManifest);

workbox.routing.registerRoute(
    /\.(?:png|gif|jpg|jpeg|svg)$/,
    workbox.strategies.staleWhileRevalidate({
        cacheName: 'images',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 60,
                maxAgeSeconds: 24 * 60 * 60, // 1 Days
            }),
        ],
    }),
);

workbox.routing.registerRoute(
    /\.(?:ttf)$/,
    workbox.strategies.cacheFirst({
        cacheName: 'font',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 30,
                maxAgeSeconds: 24 * 60 * 60, // 1 Days
            }),
        ],
    }),
);